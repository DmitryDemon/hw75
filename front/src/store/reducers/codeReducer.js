import {CODE_MESSAGE} from "../actions/actionTypes";

const initialState = {
    encode: '',
    decode: '',
    password: ''
};

const codeReducer = (state = initialState, action) => {
    switch (action.type) {
        case CODE_MESSAGE:
            return {...state, [action.text.type] : action.text.value};
        default:
            return state;
    }
};

export default codeReducer;