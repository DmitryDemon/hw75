import React, {Component, Fragment} from 'react';
import {Col, Container, FormGroup, Input, Label} from "reactstrap";
import Button from "reactstrap/es/Button";
import {inputHandler, postDecode, postEncode} from "./store/actions/codeActions";
import {connect} from "react-redux";


class App extends Component {

    inputChangeHandler = event => {
        const data = {
            type: event.target.name,
            value: event.target.value
        };
        this.props.inputHandler(data);
    };

    encodeMessage = () => {
        const data = {
            message: this.props.state.encode,
            password: this.props.state.password
        };
        this.props.postEncode(data);
    };

    decodeHandler = () => {
        const data = {
            message: this.props.state.decode,
            password: this.props.state.password
        };
        this.props.postDecode(data)
    };

    render() {
        return (
            <Fragment>
                <header>
                    <h1 style={{margin: '20px'}}>Cryptanalysis "Enigma"</h1>
                </header>
                <Container style={{marginTop: '20px'}}>


                        <FormGroup row>
                            <Label sm={2} for="decode">Decoded message</Label>
                            <Col sm={10}>
                                <Input
                                    type="text" required min="0"
                                    name="encode" id="decode"
                                    placeholder="Enter decode"
                                    value={this.props.state.encode}
                                    onChange={(e) => this.inputChangeHandler(e)}
                                />
                            </Col>
                        </FormGroup>

                        <Button onClick={this.decodeHandler} style={{margin:'30px',}}>
                            <img style={{width: '20px'}} src="https://image.freepik.com/free-icon/no-translate-detected_318-40743.jpg" alt=""/>
                        </Button >

                        <FormGroup row>
                            <Label sm={2} for="password">Password</Label>
                            <Col sm={4}>
                                <Input
                                    type="text" required min="0"
                                    name="password" id="password"
                                    placeholder="Enter password"
                                    value={this.props.state.password}
                                    onChange={(e) => this.inputChangeHandler(e)}
                                />
                            </Col>
                        </FormGroup>

                        <Button onClick={this.encodeMessage} style={{margin:'30px'}}>
                            <img style={{width: '20px'}} src="https://image.freepik.com/free-icon/no-translate-detected_318-34332.jpg" alt=""/>
                        </Button>

                        <FormGroup row style={{height: '100px'}}>
                            <Label sm={2} for="encode"><h2>Encoded message</h2></Label>
                            <Col sm={10}>
                                <Input
                                    type="text" required min="0"
                                    name="decode" id="encode"
                                    value={this.props.state.decode}
                                    onChange={(e) => this.inputChangeHandler(e)}
                                />
                            </Col>
                        </FormGroup>

                </Container>
            </Fragment>
        );
    }
}



const mapStateToProps = state => {
    return {
        state: state.code,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        inputHandler: text => dispatch(inputHandler(text)),
        postEncode: encode => dispatch(postEncode(encode)),
        postDecode: decode => dispatch(postDecode(decode))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
