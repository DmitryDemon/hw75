import {CODE_MESSAGE} from "./actionTypes";

import axios from '../../axios-api';

export const inputHandler = text => {return {type: CODE_MESSAGE, text}};




export const postEncode = code => {
    return dispatch => {
        return axios.post('encode', code).then(
            response => dispatch(inputHandler({type: 'decode', value: response.data}))
        );
    };
};

export const postDecode = code => {
    return dispatch => {
        return axios.post('decode', code).then(
            response => dispatch(inputHandler({type: 'encode', value: response.data}))
        );
    };
};